﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource MusicAudio { get; private set; }
    public AudioSource SfxAudio { get; private set; }

    public static AudioManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        MusicAudio = gameObject.AddComponent<AudioSource>();
        SfxAudio = gameObject.AddComponent<AudioSource>();

        SetAudioProps();

        PlayMusic("bg_music");
    }

    private void SetAudioProps()
    {
        MusicAudio.loop = true;
        MusicAudio.volume = 0.5f;
        MusicAudio.playOnAwake = true;

        SfxAudio.loop = false;
        SfxAudio.volume = 1;
        SfxAudio.playOnAwake = false;
    }

    public void PlayAudio(string clipName)
    {
        var clip = Resources.Load<AudioClip>("Audio/" + clipName);
        SfxAudio.PlayOneShot(clip);
    }

    public void PlayMusic(string clipName)
    {
        MusicAudio.clip = Resources.Load<AudioClip>("Audio/" + clipName);
        MusicAudio.Play();
    }
}
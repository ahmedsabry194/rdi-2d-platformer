﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public GameObject coinPrefab;

    private float yPosition;
    private int minSpawnPositionX = -8, maxSpawnPositionX = 10;

    public static CoinSpawner Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SpawnCoins(Transform targetTile)
    {
        for (int i = minSpawnPositionX; i < maxSpawnPositionX; i += 3)
        {
            if (Random.Range(0, 3) == 1)
            {
                GameObject obstacle = Instantiate(coinPrefab);
                obstacle.transform.SetParent(targetTile, true);
                yPosition = Random.Range(-0.8f, 1.5f);
                obstacle.transform.localPosition = new Vector3(i, yPosition, 0);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hedgehog : MonoBehaviour
{
    public float maxMoveX = 5;
    public float time = 5;

    private Vector3 startPosition;
    private float scale;

    private void Start()
    {
        //transform.position = new Vector3(transform.position.x, fixedY, transform.position.z);
        startPosition = transform.localPosition;
        scale = transform.localScale.x;
        StartCoroutine(MoveCoroutine());
    }

    private IEnumerator MoveCoroutine()
    {
        while (true)
        {
            #region Move Left

            float startTime = Time.time;
            Vector3 startPos = startPosition + new Vector3(maxMoveX, 0, 0);
            Vector3 targetPos = startPosition + new Vector3(-maxMoveX, 0, 0);

            transform.localScale = new Vector3(scale, scale, scale);

            while (Time.time <= startTime + time)
            {
                float t = (Time.time - startTime) / time;
                transform.localPosition = Vector3.Lerp(startPos, targetPos, t);

                yield return (null);
            }

            transform.localPosition = targetPos;

            #endregion

            // ==================================================

            #region Move Right

            startTime = Time.time;
            startPos = startPosition + new Vector3(-maxMoveX, 0, 0);
            targetPos = startPosition + new Vector3(maxMoveX, 0, 0);

            transform.localScale = new Vector3(-scale, scale, scale);

            while (Time.time <= startTime + time)
            {
                float t = (Time.time - startTime) / time;
                transform.localPosition = Vector3.Lerp(startPos, targetPos, t);

                yield return (null);
            }

            transform.localPosition = targetPos;

            #endregion
        }
    }
}
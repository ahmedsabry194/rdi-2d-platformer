﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// This script is responsible for saving the level the plaeyr is currently on.
public class LevelManager : MonoBehaviour
{
    public Levels ActiveLevel;

    public static LevelManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SelectLevel(string level)
    {
        switch (level)
        {
            case ("Level 1"):
                ActiveLevel = Levels.Level1;

                break;

            case ("Level 2"):
                ActiveLevel = Levels.Level2;

                break;

            case ("Level 3"):
                ActiveLevel = Levels.Level3;

                break;

            case ("Level 4"):
                ActiveLevel = Levels.Level4;

                break;

            case ("Level 5"):
                ActiveLevel = Levels.Level5;

                break;

            case ("Level 6"):
                ActiveLevel = Levels.Level6;

                break;
        }
    }

    public enum Levels { Level1, Level2, Level3, Level4, Level5, Level6 }
}
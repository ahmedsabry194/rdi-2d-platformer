﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    private float movementSpeed;
    private float zRotation;

    private void Awake()
    {
        movementSpeed = 10;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position += -transform.right * movementSpeed * Time.deltaTime;
    }

    private void Rotate()
    {
        transform.rotation = Quaternion.Euler(Vector3.forward * zRotation);
        zRotation += Time.deltaTime * 5;
    }
}
﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MovingObstacleSpawner : MonoBehaviour
{
    public GameObject[] obstaclePrefabs;
    public Vector3 spawnPosition;

    public static MovingObstacleSpawner Instance;

    private void Awake()
    {
        if (LevelManager.Instance.ActiveLevel == LevelManager.Levels.Level1 || LevelManager.Instance.ActiveLevel == LevelManager.Levels.Level2)
        {
            Destroy(this);
            return;
        }

        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            int obstacleIndex = UnityEngine.Random.Range(0, obstaclePrefabs.Length);
            GameObject obstacle = Instantiate(obstaclePrefabs[obstacleIndex], spawnPosition, Quaternion.identity);

            yield return (new WaitForSeconds(UnityEngine.Random.Range(5f, 8f)));
        }
    }
}
﻿using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public ObstacleData fixedObstacle;
    public ObstacleData hedgehog;
    public ObstacleData shortStep;
    public ObstacleData longStep;

    private int obstacleCountPerTile = 0;

    public static ObstacleSpawner Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SpawnObstacles(Transform targetTile)
    {
        // Level 2
        if (LevelManager.Instance.ActiveLevel == LevelManager.Levels.Level2)
        {
            SpawnFixedObstacle(targetTile, 1);
        }

        // Level 3
        if (LevelManager.Instance.ActiveLevel == LevelManager.Levels.Level3)
        {
            SpawnFixedObstacle(targetTile, 1);
            SpawnHedgehog(targetTile, 1);
        }

        // Level 4
        if (LevelManager.Instance.ActiveLevel == LevelManager.Levels.Level4)
        {
            SpawnFixedObstacle(targetTile, 1);
            SpawnHedgehog(targetTile, 2);
            SpawnShortStep(targetTile, 2);
        }

        // Level 5
        if (LevelManager.Instance.ActiveLevel == LevelManager.Levels.Level5)
        {
            SpawnFixedObstacle(targetTile, 1);
            SpawnHedgehog(targetTile, 2);
            SpawnLongStep(targetTile, 2);
        }
    }

    private void SpawnFixedObstacle(Transform targetTile, int showPossibility)
    {
        if (Random.Range(0, showPossibility) == showPossibility - 1)
        {
            GameObject obstacle = Instantiate(fixedObstacle.Prefab);
            obstacle.transform.SetParent(targetTile, true);
            obstacle.transform.localPosition = new Vector3(fixedObstacle.XPosition, fixedObstacle.YPosition, 0);
        }
    }

    private void SpawnHedgehog(Transform targetTile, int showPossibility)
    {
        if (Random.Range(0, showPossibility) == showPossibility - 1)
        {
            GameObject obstacle = Instantiate(hedgehog.Prefab);
            obstacle.transform.SetParent(targetTile, true);
            obstacle.transform.localPosition = new Vector3(hedgehog.XPosition, hedgehog.YPosition, 0);
        }
    }

    private void SpawnShortStep(Transform targetTile, int showPossibility)
    {
        if (Random.Range(0, showPossibility) == showPossibility - 1)
        {
            GameObject obstacle = Instantiate(shortStep.Prefab);
            obstacle.transform.SetParent(targetTile, true);
            obstacle.transform.localPosition = new Vector3(shortStep.XPosition, shortStep.YPosition, 0);
        }
    }

    private void SpawnLongStep(Transform targetTile, int showPossibility)
    {
        if (Random.Range(0, showPossibility) == showPossibility - 1)
        {
            GameObject obstacle = Instantiate(longStep.Prefab);
            obstacle.transform.SetParent(targetTile, true);
            obstacle.transform.localPosition = new Vector3(longStep.XPosition, longStep.YPosition, 0);
        }
    }
}

[System.Serializable]
public class ObstacleData
{
    public GameObject Prefab;
    public float XPosition;
    public float YPosition;
}
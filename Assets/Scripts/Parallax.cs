﻿using System.Collections;

using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject tileSprite;
    public float initialScollSpeed = 5;
    public float spawnRate = 4;
    public float spawnOffsetX = 18;
    public float yPosition;
    public bool isGround;

    private float currentscrollSpeed = 5;
    private float lastX;

    private void Start ()
    {
        currentscrollSpeed = initialScollSpeed;
        lastX = spawnOffsetX;
        StartCoroutine (SpawnTileCoroutine ());
    }

    private void Update ()
    {
        if (Player.Instance.IsDead)
            return;

        transform.position += -transform.right * currentscrollSpeed * Time.deltaTime;

        currentscrollSpeed += 0.25f * Time.deltaTime;
        currentscrollSpeed = Mathf.Clamp (currentscrollSpeed, initialScollSpeed, initialScollSpeed * 3);
    }

    // This routine spawns a tile every amount of time.
    private IEnumerator SpawnTileCoroutine ()
    {
        while (true)
        {
            GameObject tile = Instantiate (tileSprite, transform);
            tile.transform.localPosition = new Vector3 (lastX, yPosition, 0);

            lastX += spawnOffsetX;

            if (isGround)
            {
                if (ObstacleSpawner.Instance)
                    ObstacleSpawner.Instance.SpawnObstacles (tile.transform);

                if (StepSpawner.Instance)
                    StepSpawner.Instance.SpawnObstacles (tile.transform);

                if (StepWithObstacleSpawner.Instance)
                    StepWithObstacleSpawner.Instance.SpawnObstacles (tile.transform);

                CoinSpawner.Instance.SpawnCoins (tile.transform);
            }

            yield return (new WaitForSeconds (spawnRate));
        }
    }
}
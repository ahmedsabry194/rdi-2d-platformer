﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool IsDead { get; private set; }

    public Animator animator;
    public SpriteRenderer spriteRenderer;
    public float jumpForce = 5;
    public float jumpPoseDuration = 1;
    public AiryUIAnimationManager losePanel;
    public GameObject coinEffect;
    public GameObject hitEffect;
    public GameObject dieEffect;
    public Text coins_TXT;
    public Image[] hearts;

    private new Rigidbody2D rigidbody;
    private new Collider2D collider;
    private int coinsCount = 0;
    private int lives = 3;

    private bool isGrounded;
    public bool IsGrounded
    {
        get { return (isGrounded); }
        set
        {
            if (value != isGrounded)
                animator.SetBool("Jump", !value);

            isGrounded = value;
        }
    }
    public static Player Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Jump();
        }
    }

    // Executing Jump
    public void Jump()
    {
        if (!IsDead && IsGrounded)
        {
            AudioManager.Instance.PlayAudio("jump");

            animator.SetBool("Jump", true);
            rigidbody.velocity = transform.up * jumpForce;
        }
        if (!IsGrounded && transform.position.y <= 1.4f)
        {
            //AudioManager.Instance.PlayAudio("jump");

            rigidbody.velocity = transform.up * jumpForce;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("Obstacles") && !IsDead)
        {
            Destroy(collider);

            lives--;
            hearts[lives].color = new Color(1, 1, 1, 0.4f);

            if (lives <= 0)
            {
                OnDie();
            }
            else
            {
                OnHit();
            }
        }
        else if (collider.gameObject.layer == LayerMask.NameToLayer("Coins"))
        {
            OnGetCoin(collider.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            IsGrounded = true;
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            IsGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            IsGrounded = false;
        }
    }

    private void OnHit()
    {
        AudioManager.Instance.PlayAudio("hit");
        Instantiate(this.hitEffect, transform.position, Quaternion.identity);
    }

    // Executes when player dies.
    private void OnDie()
    {
        AudioManager.Instance.PlayAudio("die");

        Debug.Log("You Die!");

        IsDead = true;
        animator.SetTrigger("Die");

        CoroutineHandler.Instance.PlayCoroutineWithTime(() => losePanel.ShowMenu(), 2);

        GameObject dieEffect = Instantiate(this.dieEffect, transform.position, Quaternion.identity);

        gameObject.SetActive(false);
    }

    // Executes when plyer earns a coin
    private void OnGetCoin(GameObject coin)
    {
        AudioManager.Instance.PlayAudio("coin");

        coinsCount++;
        coins_TXT.text = coinsCount.ToString();

        GameObject effect = Instantiate(coinEffect, coin.transform.position, Quaternion.identity);
        Destroy(effect, 2);
        Destroy(coin);
    }
}
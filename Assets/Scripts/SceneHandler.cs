﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public string CurrentScene { get { return (SceneManager.GetActiveScene().name); } }

    public GameObject pausePanel;

    public static SceneHandler Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void LoadScene(string sceneName)
    {
        if (sceneName == "0 - Home")
        {
            Destroy(LevelManager.Instance.gameObject);
            Time.timeScale = 1;
        }
        else
        {
            LevelManager.Instance.SelectLevel(sceneName);
        }

        SceneManager.LoadScene(sceneName);
    }

    public void RestartSameLevel()
    {
        string activeSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(activeSceneName);
    }

    public void PauseGame(bool state)
    {
        pausePanel.SetActive(state);
        Time.timeScale = (state) ? 0 : 1;
    }
}
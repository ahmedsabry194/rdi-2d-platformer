﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class StepSpawner : MonoBehaviour
{
    public GameObject stepPrefab;
    public GameObject step2Prefab;
    public float xPosition1, yPosition1, xPosition2, yPosition2;

    public static StepSpawner Instance;

    private void Awake ()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SpawnObstacles (Transform targetTile)
    {
        if (Random.Range (0, 2) == 1)
        {
            GameObject obstacle = Instantiate (stepPrefab);
            obstacle.transform.SetParent (targetTile, true);
            obstacle.transform.localPosition = new Vector3 (xPosition1, yPosition1, 0);

            //if (LevelManager.Instance.ActiveLevel != LevelManager.Levels.Level5)
            //    return;

            //if (Random.Range (0, 2) == 0)
            //{
            //    GameObject obstacle2 = Instantiate (step2Prefab);
            //    obstacle2.transform.SetParent (targetTile, true);
            //    obstacle2.transform.localPosition = new Vector3 (xPosition2, yPosition2, 0);
            //}
        }
    }
}
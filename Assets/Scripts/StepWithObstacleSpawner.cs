﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class StepWithObstacleSpawner : MonoBehaviour
{
    public GameObject stepPrefab;
    public float xPosition, yPosition;

    public static StepWithObstacleSpawner Instance;

    private void Awake ()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SpawnObstacles (Transform targetTile)
    {
        if (Random.Range (0, 2) == 1)
        {
            GameObject obstacle = Instantiate (stepPrefab);
            obstacle.transform.SetParent (targetTile, true);
            obstacle.transform.localPosition = new Vector3 (xPosition, yPosition, 0);
        }
    }
}
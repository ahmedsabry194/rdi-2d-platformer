﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private GameObject tutorialPanel;
    [SerializeField] private GameObject jumpPointer;
    [SerializeField] private GameObject doubleJumpPointer;

    private void Start()
    {
        Time.timeScale = 0;
    }

    public void ShowJumpPointer()
    {
        jumpPointer.SetActive(true);
    }

    public void HideJumpPointer()
    {
        jumpPointer.SetActive(false);
    }

    public void ShowDoubleJumpPointer()
    {
        doubleJumpPointer.SetActive(true);
    }

    public void HideDoubleJumpPointer()
    {
        doubleJumpPointer.SetActive(false);
    }

    public void EndTutorial()
    {
        tutorialPanel.SetActive(false);
        Time.timeScale = 1;
    }
}